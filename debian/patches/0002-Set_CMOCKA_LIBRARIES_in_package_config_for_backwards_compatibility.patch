From de67675842e549008f3e1aeb03ae7995afa3dd26 Mon Sep 17 00:00:00 2001
From: Alexander Dahl <ada@thorsis.com>
Date: Fri, 17 Mar 2023 10:06:12 +0100
Subject: [PATCH] cmake: Set CMOCKA_LIBRARIES in package config for backwards
 compatibility

Projects using cmocka could have done this successfully from version 1.0 to
1.1.5 to build against cmocka:

```
find_package(cmocka 1.0 REQUIRED CONFIG)
```

and later

```
target_link_libraries(myapp
	${CMOCKA_LIBRARIES}
)
```

Modern apps should just "link" against 'cmocka::cmocka' instead like it's done in
examples already.  To not break old builds (as it is the case with cmocka
release 1.1.7) we can put that modern target string into the variable
CMOCKA_LIBRARIES and thus trick old projects to just use that, keeping
compatibility until those projects explicitly use the modern version.

Link: https://cmake.org/cmake/help/latest/command/install.html#install-export
Link: https://cmake.org/cmake/help/latest/manual/cmake-packages.7.html#package-configuration-file
Link: https://cmake.org/cmake/help/latest/module/CMakePackageConfigHelpers.html#module:CMakePackageConfigHelpers
Fixes: #87

Signed-off-by: Alexander Dahl <ada@thorsis.com>
Reviewed-by: Andreas Schneider <asn@cryptomilk.org>
---
 CMakeLists.txt         | 10 +++++++++-
 cmocka-config.cmake.in | 10 ++++++++++
 src/CMakeLists.txt     |  4 ++--
 3 files changed, 21 insertions(+), 3 deletions(-)
 create mode 100644 cmocka-config.cmake.in

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 8afd42c..19eb8d9 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -77,11 +77,19 @@ install(
     pkgconfig
 )
 
+configure_package_config_file(cmocka-config.cmake.in
+    "${CMAKE_CURRENT_BINARY_DIR}/cmocka-config.cmake"
+    INSTALL_DESTINATION "${CMAKE_INSTALL_LIBDIR}/cmake/cmocka"
+    NO_SET_AND_CHECK_MACRO
+    NO_CHECK_REQUIRED_COMPONENTS_MACRO
+)
+
 write_basic_package_version_file(${PROJECT_NAME}-config-version.cmake
                                  COMPATIBILITY
                                      AnyNewerVersion)
 
-install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}-config-version.cmake
+install(FILES "${CMAKE_CURRENT_BINARY_DIR}/cmocka-config.cmake"
+              "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}-config-version.cmake"
         DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}
         COMPONENT devel)
 
diff --git a/cmocka-config.cmake.in b/cmocka-config.cmake.in
new file mode 100644
index 0000000..d39bcb2
--- /dev/null
+++ b/cmocka-config.cmake.in
@@ -0,0 +1,10 @@
+@PACKAGE_INIT@
+
+get_filename_component(cmocka_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
+if(NOT TARGET cmocka::cmocka)
+	include("${cmocka_CMAKE_DIR}/cmocka-targets.cmake")
+endif()
+
+# for backwards compatibility
+set(CMOCKA_LIBRARY cmocka::cmocka)
+set(CMOCKA_LIBRARIES cmocka::cmocka)
diff --git a/src/CMakeLists.txt b/src/CMakeLists.txt
index 0fbf2d4..9e84568 100644
--- a/src/CMakeLists.txt
+++ b/src/CMakeLists.txt
@@ -60,13 +60,13 @@ set_target_properties(cmocka PROPERTIES
 add_library(cmocka::cmocka ALIAS cmocka)
 
 install(TARGETS cmocka
-        EXPORT cmocka-config
+        EXPORT cmocka-targets
         ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
         LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
         RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
         COMPONENT ${PROJECT_NAME})
 
-install(EXPORT cmocka-config
+install(EXPORT cmocka-targets
         NAMESPACE cmocka::
         DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/cmocka)
 
-- 
GitLab

